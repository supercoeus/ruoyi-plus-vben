<div align="center"> <a href="https://github.com/anncwb/vue-vben-admin"> <img alt="VbenAdmin Logo" width="200" height="200" src="https://anncwb.github.io/anncwb/images/logo.png"> </a> <br> <br>

[![license](https://img.shields.io/github/license/anncwb/vue-vben-admin.svg)](LICENSE)

<h1>RuoYi Plus Vben</h1>
</div>

## 提示

已经将 antv4 版本的代码提交到该仓库 即默认 main 分支

关于运行警告: `The CJS build of Vite's Node API is deprecated. See https://vitejs.dev/guide/troubleshooting.html#vite-cjs-node-api-deprecated for more details.` 是由于升级vite5 官方还未解决但是不影响使用 详见[vben issue](https://github.com/vbenjs/vue-vben-admin/pull/3508)

## 简介

基于 [vben(ant-design-vue)](https://github.com/vbenjs/vue-vben-admin) 的 RuoYi-Vue-Plus 前端项目

| 组件/框架      | 版本   |
| :------------- | :----- |
| vben           | 2.10.1 |
| ant-design-vue | 4.1.2  |
| vue            | 3.4.19 |

对应后端项目: **(分布式 5.X 分支 微服务 2.分支)**

分布式 [RuoYi-Vue-Plus](https://gitee.com/dromara/RuoYi-Vue-Plus/tree/5.X/)

微服务 [RuoYi-Cloud-Plus](https://gitee.com/dromara/RuoYi-Cloud-Plus/tree/2.X/)

PS: 适配JDK1.8版本 切换到[jdk1.8](https://gitee.com/dapppp/ruoyi-plus-vben/tree/jdk1.8/)分支

## 预览

admin 账号: admin admin123

[预览地址点这里](http://117.72.10.31)

## 微信群

<img src=https://117.72.10.31/prod-api/currentGroupImg height=360px />

## 文档

[vben 文档地址](https://doc.vvbin.cn/)

[RuoYi-Plus 文档地址](https://plus-doc.dromara.org/#/)

~~[本框架文档](https://gitee.com/dapppp/ruoyi-plus-vben/blob/master/docs.md)(废弃)~~

## 预览图

![图片](https://gitee.com/dapppp/ruoyi-plus-vben/raw/main/preview/1.png) ![图片](https://gitee.com/dapppp/ruoyi-plus-vben/raw/main/preview/2.png) ![图片](https://gitee.com/dapppp/ruoyi-plus-vben/raw/main/preview/3.png) ![图片](https://gitee.com/dapppp/ruoyi-plus-vben/raw/main/preview/4.png) ![图片](https://gitee.com/dapppp/ruoyi-plus-vben/raw/main/preview/5.png) ![图片](https://gitee.com/dapppp/ruoyi-plus-vben/raw/main/preview/6.png) ![图片](https://gitee.com/dapppp/ruoyi-plus-vben/raw/main/preview/7.png) ![图片](https://gitee.com/dapppp/ruoyi-plus-vben/raw/main/preview/代码预览.png)

## 安装使用

前置准备环境(推荐pnpm)

```json
"engines": {
  "node": ">=18.12.0",
  "pnpm": ">=8.10.0"
}
```

- 获取项目代码

```bash
git clone https://gitee.com/dapppp/ruoyi-plus-vben.git
```

- 安装依赖

```bash
cd ruoyi-plus-vben

pnpm install
```

使用[RuoYi-Vue-Plus](https://gitee.com/dromara/RuoYi-Vue-Plus/tree/5.X/)注意 `已经去除 admin/powerjob 的.env 配置` 可自行修改 有两种方式

1. 修改源码`/views/monitor/powerjob` `/views/monitor/admin`

```ts
// 修改地址
const url = ref<string>('http://127.0.0.1:7700/#/oms/home');
```

2. **推荐** 使用菜单自行配置 (跟 cloud 版本打开方式一致)

![图片](https://gitee.com/dapppp/ruoyi-plus-vben/raw/main/preview/菜单修改.png)

使用内嵌 iframe 方式需要解决跨域问题 可参考[nginx.conf](https://gitee.com/dromara/RuoYi-Vue-Plus/blob/5.X/script/docker/nginx/conf/nginx.conf#LC87)配置

- 修改.env.development 配置文件
- **注意 RSA 公私钥一定要修改和后端匹配**
- RSA 公私钥为两对 `前端请求加密-后端解密是一对` `后端响应加密 前端解密是一对`

```properties
# 后台请求路径 具体在vite.config.ts配置代理
VITE_GLOB_API_URL=/basic-api

# 全局加密开关(即开启了加解密功能才会生效 不是全部接口加密 需要和后端对应)
VITE_GLOB_ENABLE_ENCRYPT=true

# RSA公钥 请求加密使用 注意这两个是两对RSA公私钥 请求加密-后端解密是一对 响应解密-后端加密是一对
VITE_GLOB_RSA_PUBLIC_KEY=xxx

# RSA私钥 响应解密使用 注意这两个是两对RSA公私钥 请求加密-后端解密是一对 响应解密-后端加密是一对
VITE_GLOB_RSA_PRIVATE_KEY=xx

# 客户端id 必填
VITE_GLOB_APP_CLIENT_ID=e5cd7e4891bf95d1d19206ce24a7b32e

# 开启WEBSOCKET
VITE_GLOB_WEBSOCKET_ENABLE=true
```

- 运行

```bash
pnpm serve
```

- 打包

```bash
pnpm build
```

## 项目地址

- [vue-vben-admin](https://github.com/anncwb/vue-vben-admin) - vben
- [ruoyi-plus-vben](https://gitee.com/dapppp/ruoyi-plus-vben/tree/master)
- ~~[ruoyi-plus-vben-antv4](https://gitee.com/dapppp/ruoyi-plus-vben-antv4)~~

## 关于分支

| 分支名      | 备注                                                |
| :---------- | :-------------------------------------------------- |
| main        | 稳定版, 跟随官方最新版本                            |
| jdk1.8      | 适配jdk1.8的版本, 即官方分布式4.x分支/微服务1.x分支 |
| dev         | 开发版本 开发完毕合并到main分支                     |
| vben_latest | 周期性合并vben最新分支 然后提交到main分支           |
| master      | ant-design-vue v3版本归档                           |

## Git 贡献提交规范

- 参考 [vue](https://github.com/vuejs/vue/blob/dev/.github/COMMIT_CONVENTION.md) 规范 ([Angular](https://github.com/conventional-changelog/conventional-changelog/tree/master/packages/conventional-changelog-angular))

  - `feat` 增加新功能
  - `fix` 修复问题/BUG
  - `style` 代码风格相关无影响运行结果的
  - `perf` 优化/性能提升
  - `refactor` 重构
  - `revert` 撤销修改
  - `test` 测试相关
  - `docs` 文档/注释
  - `chore` 依赖更新/脚手架配置修改等
  - `workflow` 工作流改进
  - `ci` 持续集成
  - `types` 类型定义文件更改
  - `wip` 开发中

## 浏览器支持

本地开发推荐使用`Chrome 85+` 浏览器

支持现代浏览器, 不支持 IE

| [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_48x48.png" alt=" Edge" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>IE | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_48x48.png" alt=" Edge" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Edge | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/firefox/firefox_48x48.png" alt="Firefox" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Firefox | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/chrome/chrome_48x48.png" alt="Chrome" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Chrome | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/safari/safari_48x48.png" alt="Safari" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Safari |
| :-: | :-: | :-: | :-: | :-: |
| not support | last 2 versions | last 2 versions | last 2 versions | last 2 versions |

## 捐赠

如果项目帮助到您 可以考虑请作者喝杯咖啡 万分感谢您对开源的支持!

<img src=https://117.72.10.31:9000/plus/2024/03/16/98a9d704eb0c4c04b721bf7799217571.jpg height=360px />
