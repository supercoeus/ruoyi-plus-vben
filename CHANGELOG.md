# 1.1.10

租户套餐 增加预览菜单功能

去除表格上部表单 重置/搜索的前置图标

默认DictTag的primary色由`blue`改为`primary`

字典标签重构 支持`自定义颜色`+`css属性(前置/后置小点效果)`

unocss支持iconify图标 不用再导入Icon组件

- 即`<div class="i-material-symbols-light:13mp-outline-sharp"></div>`写法

用户管理 新增/修改 部门选择改为`级联选择器`组件

# 1.1.9

**Bug Fixed**

当存在`菜单根目录为菜单`形式(比如演示站的微信群) & login?redirect=%252F(即/) 会错误调转到`对应的第一个根目录菜单的页面`(空页面) 解决: 判断为/跳转到首页

官方的element使用ParentView来处理二级菜单 使用ParentView的菜单不应被添加到路由(添加到侧边菜单就够了)

ps: 相同name的路由 后一个会覆盖前一个(https://www.cnblogs.com/mochenxiya/p/16732793.html)

**Refactor**

主题色(primaryColor)由`#0960bd`修改为`#1677FF`[AntDesign品牌色](https://ant-design.antgroup.com/docs/spec/colors-cn#%E4%BA%A7%E5%93%81%E7%BA%A7%E8%89%B2%E5%BD%A9%E4%BD%93%E7%B3%BB)

使用Ant Design推荐的设计规范 将表格按钮从title栏(左边)移动到toolbar(右边)

详见:

- [有很多按钮组，如何确定顺序](https://ant-design.antgroup.com/docs/spec/buttons-cn#%E6%9C%89%E5%BE%88%E5%A4%9A%E6%8C%89%E9%92%AE%E7%BB%84%E5%A6%82%E4%BD%95%E7%A1%AE%E5%AE%9A%E9%A1%BA%E5%BA%8F)
- [按钮区](https://ant-design.antgroup.com/docs/spec/buttons-cn#%E6%8C%89%E9%92%AE%E5%8C%BA)

修改部分按钮颜色(比如导出)改为次要按钮

部分需要二次确认的操作(删除) 由modal改为popConfirm

- [覆盖层](https://ant-design.antgroup.com/docs/spec/stay-cn#%E8%A6%86%E7%9B%96%E5%B1%82)

代码生成-编辑 Table编辑组件改为`VxeTable` 性能对比原来的`AntTable`性能大幅提升

BasicTable 使用`reload`代替`reloadWithCallback` 官方已经修复modal会有关闭两次动画问题

**Feature**

`VITE_GLOB_ENABLE_ENCRYPT`全局请求加解密开关

TableAction的dropdown按钮支持绑定按钮样式

代码生成-代码预览 支持根据文件的不同类型切换不同的预览显示

BasicTable reload添加`doNotClearSelectRows`参数 默认reload会清空当前表格所有选中行

租户管理 `未添加任何租户套餐时`不允许打开`新增租户抽屉`并弹出提示(所有东西填完了发现没法选择租户套餐所以也没办法提交)

**其他**

客户端管理 id为1(默认PC客户端)不可进行禁用

Oss配置 新增添加提示信息

默认登录页 登录中disabled第三方登录

**依赖更新**

npm依赖升级至目前最新

# 1.1.8

**Bug Fixed**

使用filter方法替代findNodeAll(用于排除节点) (findNodeAll由于children拼写错误导致运行成功--!)

用户管理 用户导入 下载模板modal添加z-index(设置过小会有bug) 否则下载模板时会遮挡loading效果

角色管理 分配角色 导入由modal改为抽屉 修复表格翻页会重置勾选状态

**Refactor**

逻辑更新 去除websocket相关**VITE_GLOB_WEBSOCKET_URL** 兼容apiUrl为http链接/非链接形式 即使用http://aaa.com/bbb或/bbb都行

**Feature**

登录重定向 即登录页login?redirect=重定向地址(即1.1.6有bug被移除 vben官方已经修复)

- 在登录超时/踢下线/修改角色下线(即403)等一些会携带redirect /login?redirect=xxx地址
- 如果是正常登出则不带redirect /login
- redirect如果是不存在的地址(手动地址栏输入/更改角色权限导致菜单不存在)则跳转到默认首页

增加`手机号登录`的支持

**其他**

oss的图片预览组件TableImg改为异步导入 解决table加载时间过长(loading会在图片加载完成结束 改为异步则不会有限制)

大量的拼写错误(还是建议安装一个拼写检查工具 vscode没有自带 --!)

去除接口前缀相关**VITE_GLOB_API_URL_PREFIX** 直接拼接在**VITE_GLOB_API_URL**即可

# 1.1.7

**Bug Fixed**

租户套餐 在未操作菜单情况下(比如直接点确定/改备注后点确定) transformIdStr函数转number导致丢失精度 -> id直接用string

src/router/guard/permissionGuard.ts 外链不能被添加到路由(漏了)

**Feature**

租户套餐 隐藏租户相关菜单 只有superadmin可以操作 分配了也没法用

角色管理 隐藏租户相关菜单 只有superadmin可以操作 分配了也没法用

角色管理 小管理员(admin)不可操作(防止误操作把自己权限玩没)

**其他**

登录 有验证码和无验证码时input宽度(404-400px)统一 原版本在有验证码情况下input宽度太长了

用户管理 部门树选择 ->改为/

菜单管理 菜单树选择 ->改为/

部门管理 部门树选择 ->改为/

用户管理 左侧部门树增加图标

角色管理 仅超级管理员可修改小管理员菜单

# 1.1.6-fix

~~登录重定向 即登录页login?redirect=重定向地址~~ **功能有严重BUG 回滚版本**

# 1.1.6

**Bug Fixed**

修复同一个字典多次请求api(页面/modal/drawer会加载三次), 现在只会请求一次

角色管理 分配用户 Number(roleId)导致精度丢失 -> 改为string

/@/ => @/(新版vben已不支持/@/路径)

pinia-plugin-persistedstate插件无法持久化(key的问题)

**Feature**

登录页面 租户和验证码都加载完成登录按钮才可用(enable)

**有严重BUG 于1.1.6-fix版本删除** ~~登录重定向 即登录页login?redirect=重定向地址~~

- ~~在登录超时/踢下线/修改角色下线(即403)等一些会携带redirect~~
- ~~如果是正常登出则不带redirect~~
- ~~redirect如果是不存在的地址则跳转到默认首页~~

**其他**

控制台warning: [DOM] Found 2 elements with non-unique id #form_item_configKey: (More info: https://goo.gl/9p2vKq) 主要是由于id冲突(即搜索和更新用的同一个id) 表单添加上name参数即可

改了一些代码(oxlint warning) 主要是代码风格

# 1.1.5

**Bug Fixed**

无

**Feature**

无

**其他**

用户管理 默认头像

userStore 默认头像

操作日志 添加id

客户端管理 pc不允许禁用(编辑里仍然可以修改)

消息通知 style

# 1.1.4

**Bug Fixed**

夜间模式通过刷新加载会有短暂白屏问题

回滚部门管理代码 逻辑有问题 --!

**Feature**

用户管理 部门树skeleton加载

**其他**

用户管理 用户信息modal使用skeleton加载

oss配置 v-auth

代码生成 v-auth

# 1.1.3

**Bug Fixed**

无

**Feature**

用户管理 用户信息预览

添加使用modal/drawer页面打开时的loading

代码生成 - 代码预览样式优化

代码生成 - 树表

**其他**

个人中心 绑定item间距

# 1.1.2

**Bug Fixed**

通知公告 - 删除(变量写错了导致无法删除)

**Feature**

通知公告 - 预览

富文本编辑器(TinyMce)支持图片大小修改(右键修改)

富文本编辑器(TinyMce)支持图片粘贴 (base64格式非上传)

**大部分组件新增抽屉 极少数表单只有几行的没加**

部门管理 切换上级部门时 对应的负责人列表也会变化

登录日志 根据浏览器/系统名称显示对应图标

在线用户 根据浏览器/系统名称显示对应图标

**其他**

手机端不显示租户切换(有遮挡)

租户下拉框样式优化

- 用户管理-用户导入 样式更新
- 用户管理-密码修改 样式更新
- 操作日志 添加'部门'

**大部分页面支持移动端(聊胜于无)**

# 1.1.1

**Bug Fixed**

(样式)按钮点击后任然有焦点

(样式)TableSetting居中

**Feature**

操作日志支持模态框/抽屉打开 可自行选择

操作日志清空添加等待时间5S 等待完成才能点击 防止误操作

登录日志清空添加等待时间5S 等待完成才能点击 防止误操作

个人中心添加loading效果

**其他**

菜单管理 为按钮时不再显示"新增按钮"(不合逻辑)

树表(如菜单管理/部门管理)去除空children 这样前面就不会有展开/关闭图标了

代码生成-多选删除 按钮样式

CollapseContainer border-radius 2px -> 6px

缓存监控 添加图标

表格圆角 2px -> 6px

# 1.1.0 (2024年1月16日)

**依赖更新**

- 升级目前最新最新vben

**Bug Fixed**

- 修复表格在开启border情况下 左边有一条竖线的样式问题
- 修复tab的关闭按钮"X"没有居中样式问题

**Feature**

- 树表支持双击展开/折叠 -菜单管理/部门管理

**其他**

- 登录后右上角昵称默认加粗显示

# 1.0.0 (2023-11-1)

**依赖更新**

- 升级目前最新最新vben 使用antv4版本

# 0.1.0

没写 初始完成版本 使用antv3版本
