module.exports = {
  root: true,
  extends: ['@vben'],
  rules: {
    'no-undef': 'off',
    /** 不检查空的代码块 比如catch */
    'no-empty': 0,
  },
};
